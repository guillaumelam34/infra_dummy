FROM python:3

RUN apt update
RUN apt install -y bash vim

WORKDIR /user/src/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT [ "python" ]

CMD ["./main.py"]
