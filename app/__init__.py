# flake8: noqa
from flask import Flask, render_template
import flask

app = Flask(__name__)

@app.route('/')
def home():
    return "Hello World!"


@app.route('/template')
def template():
    return render_template('home.html')


@app.route('/healthz')
def healthz():
    return 'HEALTHY'

