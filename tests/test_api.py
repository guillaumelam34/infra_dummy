def test_home(client):
    result = client.get('/')
    assert result.status_code == 200
    assert result.data.decode('utf-8') == "Hello World!"


def test_template(client):
    result = client.get('/template')
    assert result.status_code == 200
    assert '<h2>FLASK APP DEPLOYED TO AWS USING DOCKER V4</h2>' in result.data.decode('utf-8')
