#!/bin/bash

# treat undefined variables as an error, and exit script immediately on error
set -eux

CONTAINER_NAME="$1"
IMAGE_NAME="$2"
SMOKE_TEST_IMAGE="$CONTAINER_NAME-smoke-test"

TEST_PORT=80
MAX_WAIT_SECONDS=30

# kill any lingering test containers
docker kill $SMOKE_TEST_IMAGE || true

docker run -d --rm --name=$SMOKE_TEST_IMAGE ${IMAGE_NAME}

while true
do
    set +e
    health_check=$(docker run --rm --network container:$SMOKE_TEST_IMAGE appropriate/curl http://localhost:${TEST_PORT}/healthz)
    set -e

    if echo $health_check | grep -q "HEALTHY"; then
        echo "Local health check passed"
        docker kill $SMOKE_TEST_IMAGE
        exit 0
    fi
        
    echo "waiting for container to startup..."
    sleep 1.0
    ((MAX_WAIT_SECONDS=MAX_WAIT_SECONDS-1)) #decrement
    if [ $MAX_WAIT_SECONDS -le 0 ]; then
        echo "Local health check failed, port $TEST_PORT not available"
        docker kill $SMOKE_TEST_IMAGE || true
        exit -1
    fi
done